# Local testing

## Games

1. run `hugo server`
1. open `http://localhost:1313/games/$GAME/` e.g.: http://localhost:1313/games/gntm/

## Ingress generation

1. run `./generate-ingresses.sh`
1. files are generated under `./kubernetes`
