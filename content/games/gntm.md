---
title: GNTM&trade; the Drinking Game
description: Das Trinkspiel, das Germany's next Topmodel&trade; erträglich macht.

subdomain: gntm
theme:
  name: "topmodel"
  color: "#C2185B"
copyright:
  - subjects: ["GNTM", "Germany's next Topmodel"]
    holder: "ProSiebenSat.1 TV Deutschland GmbH"
---

# GNTM&trade; the Drinking Game

## Die Regeln

In folgenden Fällen ist **ein Schluck** zu nehmen:

- Jemand sagt "**Mädchen**"
- Jemand sagt "**Foto**"
- Heidi sagt: "**super**"
- Heidi ist überbelichtet
- Jemand **heult**
- Jemand verwendet einen unnötigen Anglizismus (meist "**Shooting**", "**Competition**" oder "**Attitude**")
- Eine Bitch lästert über eine andere Bitch in die Kamera

In folgenden Fällen ist **exen** angesagt:

- Heidi sagt: "**Nur eine kann Germany's next Topmodel werden**"

{{< theme-switch options="topmodel:#C2185B:Topmodel blue:#303F9F:Blau green:#689F38:Grün" >}}
